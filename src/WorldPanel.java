import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.TimerTask;

/**
 * Created by pankaj on 21/9/15.
 */
public class WorldPanel extends JPanel implements MouseListener{
    Board b;
    boolean running;
    static final int TargetFPS = 10;
    final int R = 20;
    java.util.Timer timer;
    WorldPanel() {
        super();
        b = new Board();
        running = false;
    }

    long times[]=new long[50];
    double ups;
    int index;

    public void start() {
        if (!running) {
            timer = new java.util.Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    long pt = times[index];
                    times[index] = System.currentTimeMillis();
                    ups = 1000.0*times.length/((times[index++] - pt));
                    index = index % times.length;
                    b.nextGeneration();
                    repaint();
                }
            }, 0, 1000 / TargetFPS);
            index=0;
            running=true;
        }
    }

    public void stop() {
        timer.cancel();
        running=false;
    }

    @Override
    public void paintComponent(Graphics g) {
        g.clearRect(0, 0, getWidth(), getHeight());
        int w = getWidth() / R;
        int h = getHeight() / R;
        for (int i = 0; i <= w; i++)
            g.drawLine(i * R, 0, i * R, getHeight());
        for (int i = 0; i <= h; i++)
            g.drawLine(0, i * R, getWidth(), i * R);
        for (Board.cell x : b.alive)
            if (x.x < w && x.x >= 0 && x.y <= h && x.y >= 0)
                g.fillRect(x.x * R, x.y * R, R, R);
        g.drawString("ups:"+ups,10,20);
    }

    public static void main1(String argv[]) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {

        }
        JFrame j = new JFrame("Conwey's Game of Life");
        j.setSize(500, 500);
        j.setLayout(new BorderLayout());
        WorldPanel p = new WorldPanel();
        p.addMouseListener(p);
        j.add(p, BorderLayout.CENTER);
        JButton adv = new JButton("Advance");
        JButton sch = new JButton("Start");
        JButton stp = new JButton("Stop");
        Box top = Box.createHorizontalBox();
        System.out.println("Hello World");
        top.add(adv);
        top.add(sch);
        top.add(stp);
        j.add(top, BorderLayout.NORTH);
        adv.addActionListener((e) -> {
            p.b.nextGeneration();
            p.repaint();
        });
        sch.addActionListener((e) -> {
            p.start();
        });
        stp.addActionListener((e) -> {
            p.stop();
        });
        j.setVisible(true);
        j.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        b.flip(e.getX() / R, e.getY() / R);
        repaint();
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

}
