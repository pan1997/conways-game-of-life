import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;

/**
 * Created by pankaj on 21/9/15.
 */
public class Board {
    LinkedHashSet<cell> alive;
    LinkedHashSet<cell> prev;

    Board() {
        alive = new LinkedHashSet<>();
        prev = new LinkedHashSet<>();

    }
    /*
     * generates the next generation of the current board. The board is overwritten in the process.
     */
    void nextGeneration() {
        LinkedHashMap<cell, Integer> neighbours = new LinkedHashMap<>();
        LinkedHashSet<cell> t = alive;
        alive = prev;
        prev = t;
        Integer n;
        cell l;
        for (cell x : prev) {
            l = new cell(x.x + 1, x.y);
            if (neighbours.containsKey(l)) neighbours.put(l, neighbours.get(l) + 1);
            else neighbours.put(l, 1);
            l = new cell(x.x - 1, x.y);
            if (neighbours.containsKey(l)) neighbours.put(l, neighbours.get(l) + 1);
            else neighbours.put(l, 1);
            l = new cell(x.x, x.y + 1);
            if (neighbours.containsKey(l)) neighbours.put(l, neighbours.get(l) + 1);
            else neighbours.put(l, 1);
            l = new cell(x.x, x.y - 1);
            if (neighbours.containsKey(l)) neighbours.put(l, neighbours.get(l) + 1);
            else neighbours.put(l, 1);
            l = new cell(x.x + 1, x.y + 1);
            if (neighbours.containsKey(l)) neighbours.put(l, neighbours.get(l) + 1);
            else neighbours.put(l, 1);
            l = new cell(x.x + 1, x.y - 1);
            if (neighbours.containsKey(l)) neighbours.put(l, neighbours.get(l) + 1);
            else neighbours.put(l, 1);
            l = new cell(x.x - 1, x.y + 1);
            if (neighbours.containsKey(l)) neighbours.put(l, neighbours.get(l) + 1);
            else neighbours.put(l, 1);
            l = new cell(x.x - 1, x.y - 1);
            if (neighbours.containsKey(l)) neighbours.put(l, neighbours.get(l) + 1);
            else neighbours.put(l, 1);
        }
        //System.out.println(neighbours);
        alive.clear();
        for (Map.Entry<cell, Integer> x : neighbours.entrySet()) {
            if (x.getValue() == 3)
                alive.add(x.getKey());
            else if (x.getValue() == 2 && prev.contains(x.getKey()))
                alive.add(x.getKey());
        }
    }
    /*
     * filps the state of a cell. Alive <--> Dead
     */
    void flip(int x, int y) {
        cell c = new cell(x, y);
        if (alive.contains(c))
            alive.remove(c);
        else alive.add(c);
    }
    /*
     * A test for the board program with sample use case
    `*/
    public static void main(String argv[])throws Exception {
        BufferedReader bin = new BufferedReader(new InputStreamReader(System.in));
        String s = bin.readLine();
        //int n = Integer.parseInt(bin.readLine());
        for(int n=1;n<26;n++) {
            for (int i = 0; i < s.length(); i++) {
                char ch = s.charAt(i);
                if (ch <= 'z' && ch >= 'a') {
                    ch = (char) ('a' + (26 + ch - 'a' + n) % 26);
                }
                System.out.print(ch);
            }
            System.out.println("  :"+n);
        }
    }

    class cell {
        int x, y;

        cell(int a, int b) {
            x = a;
            y = b;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj instanceof cell) {
                cell a = (cell) obj;
                return a.x == x && a.y == y;
            }
            return false;
        }

        @Override
        public int hashCode() {
            return Integer.hashCode(x) + 53 * Integer.hashCode(y);
        }

        @Override
        public String toString() {
            return "(" + x + "," + y + ")";
        }
    }
}