import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.util.TimerTask;


/**
 * Created by pankaj on 22/9/15.
 */
public class FXMain extends Application {
    Canvas display;
    GraphicsContext gc;
    Board b;
    java.util.Timer timer;
    static final int R = 20;
    static final int targetFps=20;
    double ups;
    boolean running;
    public FXMain() {
        display = new Canvas(500, 500);
        gc = display.getGraphicsContext2D();
        display.widthProperty().addListener(o->repaint());
        display.heightProperty().addListener(o->repaint());
        b = new Board();
        ups = 0;
        running=false;
    }
    void startSimul(){
        if(!running){
            timer = new java.util.Timer(true);
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    long pt = times[index];
                    times[index] = System.currentTimeMillis();
                    ups = 1000.0*times.length/((times[index++] - pt));
                    index = index % times.length;
                    b.nextGeneration();
                    repaint();
                }
            }, 0, 1000 / targetFps);
            index=0;
            running=true;
        }
    }
    void stopSimul(){
        if(running){
            running=false;
            timer.cancel();
        }
    }
    void repaint() {
        gc.clearRect(0, 0, display.getWidth(), display.getHeight());
        double w = display.getWidth() / R;
        double h = display.getHeight() / R;
        for (int i = 0; i <= w; i++)
            gc.strokeLine(i * R, 0, i * R, display.getHeight());
        for (int i = 0; i <= h; i++)
            gc.strokeLine(0, i * R, display.getWidth(), i * R);
        for (Board.cell x : b.alive)
            if (x.x < w && x.x >= 0 && x.y <= h && x.y >= 0)
                gc.fillRect(x.x * R, x.y * R, R, R);
        gc.strokeText("ups:" + ups, 10, 20);
    }
    long[] times=new long[10];
    int index=0;
    void advance() {
        b.nextGeneration();
        long p=times[index];
        times[index]=System.currentTimeMillis();
        ups=1000.0*times.length/(times[index++]-p);
        index=index%times.length;
        Platform.runLater(this::repaint);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("Conway's Game of Life");
        BorderPane root = new BorderPane();
        root.setCenter(display);
        Button adv = new Button("Advance");
        Button stt = new Button("Start");
        Button stp = new Button("Stop");
        ToolBar topBar = new ToolBar(adv, stt, stp);
        root.setTop(topBar);
        adv.setOnAction((e) -> {
            advance();
            repaint();
        });
        stt.setOnAction(e->startSimul());
        stp.setOnAction(e->stopSimul());
        display.widthProperty().bind(root.widthProperty());
        display.heightProperty().bind(root.heightProperty().subtract(38));
        display.setOnMouseClicked((e) -> {
            b.flip((int) (e.getX() / R), (int) (e.getY() / R));
            repaint();
        });
        primaryStage.setScene(new Scene(root, 500, 500));
        primaryStage.show();
    }

    public static void main(String argv[]) {
        launch(argv);
    }
}
